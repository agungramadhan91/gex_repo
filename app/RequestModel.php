<?php

namespace App;

use App\JobSheet;
use Illuminate\Database\Eloquent\Model;

class RequestModel extends Model
{
    //
    protected $table = 'requests';
    protected $fillable = ['jobsheet_id','pay_id','tanggal','user_id','tanggal','status','type'];

    public function jobsheet()
    {
    	return $this->belongsTo('App\JobSheet');
    }

    public function payable()
    {
        return $this->hasOne('App\Payable', 'id','pay_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
