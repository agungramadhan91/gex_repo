@extends('layouts.app')

@section('title', 'Request')

@section('content')
	<div class="main-content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-md-12">
	                <!-- JOB SHEET -->
	                <div class="panel">
	                    <div class="panel-body no-padding">
	                        <div role="tabpanel">
	                            <!-- Nav tabs -->
	                            <ul class="nav nav-tabs nav-tabs-1" role="tablist">
	                                <li role="presentation" class="active">
	                                    <a href="#jobsheetlist" aria-controls="jobsheetlist" role="tab" data-toggle="tab">COMPLETED JOBSHEET</a>
	                                </li>
	                            </ul>

								@include('message.error')

	                            <!-- Tab panes -->
	                            <div class="tab-content tab-content-1">
	                                <div role="tabpanel" class="tab-pane active" id="jobsheetlist">
	                                    <div class="table-responsive">

											@if(Auth::user()->role != 'admin2')
												<button id="create-request" class="btn btn-success btn-xs add-modal m-b-10 hidden">
													<i class="fa fa-plus" data-toggle="tooltip" title="Create Request from Selected Jobsheets"></i> Create Request
												</button>
												<hr>
											@endif
	                                        <table class="table table-bordered table-body-condensed table-striped table-hover" id="myTables">
	                                            <thead>  
										          <tr>
										          	<th>NO.</th>  
										            <th>JOB</th>  
										            <th>DATE</th>
										            <th>CUSTOMER</th>
										            <th>MARKETING</th>
										            <th>ORIGIN</th>
										            <th>DESTINATION</th>
										            <th>ETD</th>
										            <th>ETA</th>
													  <th title="RCS (COUNT)">RCS (COUNT)</th>
													  <th>ACTION</th>
										          </tr>  
										        </thead>
										        <?php $no = 1; ?>  
										        <tbody>  
										        	@foreach($jobsheets->sortByDesc('id') as $jobsheet)
											            @php
											            	$customer_id = App\MasterCustomer::find($jobsheet->customer_id);
											            	$marketing_id = App\User::find($jobsheet->marketing_id);
											            	$poo_id = App\MasterPort::find($jobsheet->poo_id);
											            	$pod_id = App\MasterPort::find($jobsheet->pod_id);
											            	$cek = App\Invoice::where('jobsheet_id', $jobsheet->id)->count();
															$rcsCount = App\RC::where('jobsheet_id', $jobsheet->getKey())->count();
														@endphp
												         <tr>
												          	<td>{{ $no }}</td>  
												            <td>
												            	@if(Auth::user()->role == 'marketing' || Auth::user()->role == 'admin')
												            		<a href="{{ route('marketing.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@elseif(Auth::user()->role == 'marketing' || Auth::user()->role == 'admin')
												            		<a href="{{ route('marketing.jobsheet.show', $jobsheet->id) }}"> {{ $jobsheet->code }} </a>
												            	@elseif(Auth::user()->role == 'pricing' || Auth::user()->role == 'admin')
												            		<a href="{{ route('pricing.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@elseif(Auth::user()->role == 'payable' || Auth::user()->role == 'admin')
												            		<a href="{{ route('payable.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@elseif(Auth::user()->role == 'pajak' || Auth::user()->role == 'admin')
												            		<a href="{{ route('pajak.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@endif
												            </td>  
												            <td>{{ $jobsheet->date }}</td>
												            <td>{{ $customer_id->name }}</td>  
												            <td>{{ $marketing_id->name }}</td>
												            <td>{{ $poo_id->nick_name }}</td>
												            <td>{{ $pod_id->nick_name }}</td>
												            <td>{{ $jobsheet->eta }}</td>
												            <td>{{ $jobsheet->etd }}</td>
															 <td>{{ $rcsCount }}</td>
															 <td class="text-center">
																 <a href="{{ route('marketing.request-rc.detail-jobsheet', ['id'=>$jobsheet->id]) }}"><i class="fa fa-eye"></i></a>
															 </td>
												          </tr>
												          <?php $no++; ?>  
										          	@endforeach
										        </tbody>  
	                                        </table>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <!-- END JOB SHEET -->
	            </div>
	        </div>
	    </div>
	</div>

@endsection

@section('script')
	
	<script>
		$(document).ready(function(){
			$('#myTables').dataTable({
				//"order": [[ 3, "desc" ]],
				"columnDefs": [
					{ "sortable": false,  "targets": [ 9 ] }
				]
			});
		});
	</script>

@endsection