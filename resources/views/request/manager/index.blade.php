@extends('layouts.app')

@section('title', 'Request')

@section('content')

	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<!-- JOB SHEET -->
					<div class="panel">
						<div class="panel-body no-padding">
							<div role="tabpanel">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs nav-tabs-1" role="tablist">
									<li role="presentation" class="active">
										<a href="#jobsheetlist" aria-controls="jobsheetlist" role="tab" data-toggle="tab">LIST REQUEST</a>
									</li>
								</ul>

								@include('message.info')

										<!-- Tab panes -->
								<div class="tab-content tab-content-1">
									<div role="tabpanel" class="tab-pane active" id="jobsheetlist">
										<div class="table-responsive">

											<div class="row">
												<div class="col-sm-12 pull-left">
													{!! Form::open(['method' => 'GET','class'=>'form-inline']) !!}
													<div class="form-group">
														<input type="text" name="date_from" placeholder="Date From" class="input-sm form-control datepicker-from" value="{!! $dateForm !!}">
														<input type="text" name="date_to" placeholder="Date To" class="input-sm form-control datepicker-to" value="{!! $dateTo !!}">
														<select name="request_type" class="form-control">
															<option value="" >Select Type</option>
															<option value="payable" >Payable</option>
															<option value="rc" >RC</option>
														</select>
														{{--<select name="request_curr" class="form-control">
															<option value="" >Select Curr</option>
															<option value="1" >IDR</option>
															<option value="2" >USD</option>
														</select>--}}
														<button class="btn btn-sm btn-primary clearfix">Filter</button>
													</div>
													{!! Form::close() !!}
												</div>
											</div>
											<div style="margin-bottom: 10px !important;"></div>

											@if( isset( $isApprovable ) && $isApprovable )
												{!! Form::open(['url'=>route('payable.request.submit-approvable'), 'method' => 'POST','class'=>'form-inline bulk_grid_form']) !!}
											@endif
													<table class="table table-bordered table-body-condensed table-striped table-hover" id="@if( isset( $isApprovable ) && $isApprovable ){!! 'requestedTable' !!}@else{!! 'myTables' !!}@endif">
														<thead>
														<tr>
															@if( isset( $isApprovable ) && $isApprovable )
																<th class="valign-middle text-center">
																	<input type="checkbox" name="request_all_id" value="" class="request_all_id">
																</th>
															@endif
															<th>NO.</th>
															<th>PAYMENT TYPE</th>
															<th>BANK</th>
															<th>REQUEST CURR</th>
															<th>PAYMENT CURR</th>
															<th>RATE</th>
															<th>JOB</th>
															<th>DATE REQUEST</th>
															<th>DOCUMENT</th>
															<th>USER</th>
															<th>TOTAL</th>
															{{--<th>CODE</th>--}}
															<th>TYPE</th>
															{{--<th>ACTION</th>--}}
														</tr>
														</thead>
														<?php $no = 1; ?>
														<tbody>
														@foreach($requests as $request)
															@php
															// $job = App\Jobsheet::find($request->jobsheet_id);
															// $customer_id = App\MasterCustomer::find($job->customer_id);
															// $poo_id = App\MasterPort::find($job->poo_id);
															// $pod_id = App\MasterPort::find($job->pod_id);

															{{--$pay = App\Payable::find($request->payable_id);
															$doc = App\MasterDocument::find($pay->document_id);
															$vendor = App\MasterVendor::find($pay->vendor_id);--}}

															$masterCurr = $request->currency();
															$currId = '';
															$currName = '';
															$requestCurrencyAttribute = [];
															if( $masterCurr ){
																$currName = $masterCurr->name;
																$currId = $masterCurr->id;
															}
															$docName = $request->document_id;
															$totalAmount = 0;
															$currOld = '';
															$currNew = '';
															if( $request->payable_id ) {
																if( $request->payable ) {
																	if( $request->payable->document ) {
																		$docName = $request->payable->document->name;
																	}
																	$totalAmount = $request->payable->total;
																	$currOld = $request->payable->currency;
																}
															} else {
																if( $request->rc ) {
																	if( $request->rc->document ) {
																		$docName = $request->rc->document->name;
																	}
																	$totalAmount = $request->rc->total;
																	$currOld = $request->rc->currency;
																}
															}

															if( $totalAmount == '' ){
																$totalAmount = 0;
															}

															@endphp
															<tr>
																@if( isset( $isApprovable ) && $isApprovable )
																	<td class="text-center">
																		<input type="checkbox" name="request_ids[]" value="{!! $request->getKey() !!}" class='request_ids'>
																	</td>
																@endif
																	<td>{{ $no }}</td>
																	<td>{{ $request->payment_type }}</td>
																	<td>{{ $request->bank ? $request->bank->name : '' }}</td>
																	<td>{{ $request->payable }}</td>
																	<td>{{ $no }}</td>
																	<td>{{ $request->jobsheet ? $request->jobsheet->code : '' }}</td>
																	<td>{{ $request->jobsheet ? $request->jobsheet->code : '' }}</td>
																	<td>{{ $request->tanggal }}</td>
																	<td>{{ $docName }}</td>
																	<td>{{ $request->user && $request->user->name ? $request->user->name : $request->user_id }}</td>
																	<td class="request-total" data-old="{!! $totalAmount !!}">
																			{!! $currName !!}
																			{!! \App\Helpers\ArzFormatPrice::price($totalAmount,$currId) !!}</td>
																	{{--<td>{{ $request->rc && $request->rc->code ? $request->rc->code : '' }}</td>--}}
																	<td>{{ $request->type }}</td>
															</tr>
															<?php $no++; ?>
														@endforeach
														</tbody>
													</table>

											@if( isset( $isApprovable ) && $isApprovable )
												<input type="hidden" id="request_status" name="request_status" value="approved">
												<button id="create-approvable" class="btn btn-success btn-xs add-modal m-b-10 hidden">
													<i class="fa fa-plus" data-toggle="tooltip" title="Approvable from Selected Charges"></i> Approvable
												</button>
												{!! Form::close() !!}
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END JOB SHEET -->
				</div>
			</div>
		</div>
	</div>

@endsection

@section('script')

	<script src="{{ asset('vendor/bootstrap/js/bootstrap-datepicker.js') }}"></script>
	<script>
		$(document).ready(function(){
			function numberWithCommas(x, c) {

				if( c == 1 ){
					x = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
				} else {
					x = Math.round(x * 100) / 100;
					if( x > 1 ) {
						x = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					}
				}

				return x;
			}
			var dateFormat = 'yyyy-mm-dd',
					rangeFrom = $('.datepicker-from')
							.datepicker({
								autoclose: 'true',
								//setDate: new Date(),
								//todayHighlight: 'true',
								format: dateFormat
							}).on( "changeDate", function() {
								console.log(getDate( $(this) ));
								rangeTo.datepicker( "setStartDate", getDate( $(this) ) );
							}),
					rangeTo = $('.datepicker-to')
							.datepicker({
								autoclose: 'true',
								//setDate: new Date(),
								//todayHighlight: 'true',
								format: dateFormat
							}).on( "changeDate", function() {
								rangeFrom.datepicker( "setEndDate", getDate( $(this) ) );
							});

			function getDate( element ) {
				var date;
				try {
					date = element.val();
				} catch( error ) {
					date = null;
				}

				return date;
			}

			var body = $('body');
			function toggleShowBtnCreate(){
				var requestSelectedIds = body.find('.request_ids:checked');
				var requestBtnCreate = $('#create-approvable');
				requestBtnCreate.addClass('hidden');
				if( requestSelectedIds.length > 0 ){
					requestBtnCreate.removeClass('hidden');
				}
			}

			body.on('click', '.request_all_id', function () {
				var $thisChecked = $(this).prop("checked");
				var checkBoxes = $('.request_ids');
				checkBoxes.prop("checked", $thisChecked);
				toggleShowBtnCreate();
			});

			body.on('click', '.request_ids', function () {
				toggleShowBtnCreate();
			});

			/*$('.bulk_grid_form').on('submit', function(e){
				var $this = $(this);
				var gridIds = body.find('.request_ids:checked');
				var requestStatus = $('.request_status').val();
				if( requestStatus != '' && gridIds.length > 0 ) {
					var tmpGridIds = [];
					$.each(gridIds, function () {
						if( $(this).val() )
							tmpGridIds.push($(this).val());
					});
					if(tmpGridIds) {
						$('#request_all_ids').val(tmpGridIds.join(",", tmpGridIds));
						return true;
					}
				}

				return false;
			});*/


			$('#requestedTable').dataTable({
				"order": [[ 1, "asc" ]],
				"columnDefs": [
					{ "sortable": false,  "targets": [ 0 ] }
				]
			});
			body.find('.request-payment-type').on('change', function(){
				var requestTr = $(this).closest('tr');
				var requestBank = requestTr.find('.request-bank');
				requestBank.val("");
				requestBank.prop("disabled", "disabled");
				requestBank.prop("required",false);
				if( $(this).val() == "BANK" ) {
					requestBank.prop("disabled", false);
					requestBank.prop("required",true);
				}
			}).trigger('change');

			body.find('.request-payment-currency').on('change', function(){
				var requestTr = $(this).closest('tr');
				var requestRate = requestTr.find('.request-rate');
				var requestCurrOld = requestRate.attr('data-old');
				var requestTotal = requestTr.find('.request-total');
				var total = requestTotal.attr('data-old');
				requestRate.prop("disabled", true);
				requestRate.val(1);
				if( $(this).val() == 1 && requestCurrOld != 1 ) {
					requestRate.val(requestRate.attr('data-rToday'));
					total = requestRate.attr('data-rToday')*requestTotal.attr('data-old');
					requestRate.prop("disabled", false);
				}
				if( $(this).val() == 2 && requestCurrOld != 2 ) {
					requestRate.val(requestRate.attr('data-rToday'));
					total = requestTotal.attr('data-old')/requestRate.attr('data-rToday');
					requestRate.prop("disabled", false);
				}
				//console.log(total);
				requestTotal.html(numberWithCommas(total,$(this).val()));
			}).trigger('change');

			$('#myTables').dataTable();

		});
	</script>

@endsection