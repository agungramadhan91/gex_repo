@extends('layouts.app')

@section('title', 'Request')

@section('content')
	<div class="main-content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-md-12">
	                <!-- JOB SHEET -->
	                <div class="panel">
	                    <div class="panel-body no-padding">
	                        <div role="tabpanel">
	                            <!-- Nav tabs -->
	                            <ul class="nav nav-tabs nav-tabs-1" role="tablist">
	                                <li role="presentation" class="active">
	                                    <a href="#jobsheetlist" aria-controls="jobsheetlist" role="tab" data-toggle="tab">COMPLETED JOBSHEET</a>
	                                </li>
	                            </ul>

								@include('message.error')

	                            <!-- Tab panes -->
	                            <div class="tab-content tab-content-1">
	                                <div role="tabpanel" class="tab-pane active" id="jobsheetlist">
	                                    <div class="table-responsive">

											<div class="row">
												<div class="col-sm-6 pull-left">
													{!! Form::open(['method' => 'GET','class'=>'form-inline']) !!}
													<div class="form-group">
														<input type="text" name="date_from" class="input-sm form-control datepicker-from" value="{!! $dateForm !!}">
														<input type="text" name="date_to" class="input-sm form-control datepicker-to" value="{!! $dateTo !!}">
														<button class="btn btn-sm btn-primary clearfix">Filter</button>
													</div>
													{!! Form::close() !!}
												</div>
											</div>
											<div style="margin-bottom: 10px !important;"></div>

	                                        <table class="table table-bordered table-body-condensed table-striped table-hover" id="myTables">
	                                            <thead>  
										          <tr>
										          	<th>NO.</th>  
										            <th>JOB</th>  
										            <th>DATE</th>
										            <th>CUSTOMER</th>
										            <th>MARKETING</th>
										            <th>ORIGIN</th>
										            <th>DESTINATION</th>
										            <th>ETD</th>
										            <th>ETA</th>
													  <th title="CHARGES (COUNT)">CHARGES (COUNT)</th>
													  <th>ACTION</th>
										          </tr>  
										        </thead>
										        <?php $no = 1; ?>  
										        <tbody>  
										        	@foreach($jobsheets->sortByDesc('id') as $jobsheet)
											            @php
											            	$customer_id = App\MasterCustomer::find($jobsheet->customer_id);
											            	$marketing_id = App\User::find($jobsheet->marketing_id);
											            	$poo_id = App\MasterPort::find($jobsheet->poo_id);
											            	$pod_id = App\MasterPort::find($jobsheet->pod_id);
											            	$cek = App\Invoice::where('jobsheet_id', $jobsheet->id)->count();
															$payableCount = App\Payable::where('jobsheet_id', $jobsheet->getKey())->count();
											            @endphp
												         <tr>
												          	<td>{{ $no }}</td>  
												            <td>
												            	@if(Auth::user()->role == 'operation' || Auth::user()->role == 'admin')
												            		<a href="{{ route('operation.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@elseif(Auth::user()->role == 'marketing' || Auth::user()->role == 'admin')
												            		<a href="{{ route('marketing.jobsheet.show', $jobsheet->id) }}"> {{ $jobsheet->code }} </a>
												            	@elseif(Auth::user()->role == 'pricing' || Auth::user()->role == 'admin')
												            		<a href="{{ route('pricing.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@elseif(Auth::user()->role == 'payable' || Auth::user()->role == 'admin')
												            		<a href="{{ route('payable.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@elseif(Auth::user()->role == 'pajak' || Auth::user()->role == 'admin')
												            		<a href="{{ route('pajak.jobsheet.show', $jobsheet->id) }}">{{ $jobsheet->code }}</a>
												            	@endif
												            </td>  
												            <td>{{ $jobsheet->date }}</td>
												            <td>{{ $customer_id->name }}</td>  
												            <td>{{ $marketing_id->name }}</td>
												            <td>{{ $poo_id->nick_name }}</td>
												            <td>{{ $pod_id->nick_name }}</td>
												            <td>{{ $jobsheet->eta }}</td>
												            <td>{{ $jobsheet->etd }}</td>
															 <td>{{ $payableCount }}</td>
															 <td class="text-center">
																 <a href="{{ route('payable.request.detail-jobsheet', ['id'=>$jobsheet->id]) }}"><i class="fa fa-eye"></i></a>
															 </td>
												          </tr>
												          <?php $no++; ?>  
										          	@endforeach
										        </tbody>  
	                                        </table>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <!-- END JOB SHEET -->
	            </div>
	        </div>
	    </div>
	</div>

@endsection

@section('script')

	<script src="{{ asset('vendor/bootstrap/js/bootstrap-datepicker.js') }}"></script>
	<script>
		$(document).ready(function(){
			var dateFormat = 'yyyy-mm-dd',
				rangeFrom = $('.datepicker-from')
						.datepicker({
							autoclose: 'true',
							//setDate: new Date(),
							//todayHighlight: 'true',
							format: dateFormat
						}).on( "changeDate", function() {
							console.log(getDate( $(this) ));
							rangeTo.datepicker( "setStartDate", getDate( $(this) ) );
						}),
				rangeTo = $('.datepicker-to')
						.datepicker({
							autoclose: 'true',
							//setDate: new Date(),
							//todayHighlight: 'true',
							format: dateFormat
						}).on( "changeDate", function() {
							rangeFrom.datepicker( "setEndDate", getDate( $(this) ) );
						});

			function getDate( element ) {
				var date;
				try {
					date = element.val();
				} catch( error ) {
					date = null;
				}

				return date;
			}
			$('#myTables').dataTable({
				//"order": [[ 3, "desc" ]],
				"columnDefs": [
					{ "sortable": false,  "targets": [ 10 ] }
				]
			});
		});
	</script>

@endsection